﻿




// Generated by PIDL compiler.
// Do not modify this file, but modify the source .pidl file.

using System;
using System.Net;

namespace C2S
{
	internal class Proxy:Nettention.Proud.RmiProxy
	{
public bool Chat(Nettention.Proud.HostID remote,Nettention.Proud.RmiContext rmiContext, System.String a, int b, float c)
{
	Nettention.Proud.Message __msg=new Nettention.Proud.Message();
		__msg.SimplePacketMode = core.IsSimplePacketMode();
		Nettention.Proud.RmiID __msgid= Common.Chat;
		__msg.Write(__msgid);
		Nettention.Proud.Marshaler.Write(__msg, a);
		Nettention.Proud.Marshaler.Write(__msg, b);
		Nettention.Proud.Marshaler.Write(__msg, c);
		
	Nettention.Proud.HostID[] __list = new Nettention.Proud.HostID[1];
	__list[0] = remote;
		
	return RmiSend(__list,rmiContext,__msg,
		RmiName_Chat, Common.Chat);
}

public bool Chat(Nettention.Proud.HostID[] remotes,Nettention.Proud.RmiContext rmiContext, System.String a, int b, float c)
{
	Nettention.Proud.Message __msg=new Nettention.Proud.Message();
__msg.SimplePacketMode = core.IsSimplePacketMode();
Nettention.Proud.RmiID __msgid= Common.Chat;
__msg.Write(__msgid);
Nettention.Proud.Marshaler.Write(__msg, a);
Nettention.Proud.Marshaler.Write(__msg, b);
Nettention.Proud.Marshaler.Write(__msg, c);
		
	return RmiSend(remotes,rmiContext,__msg,
		RmiName_Chat, Common.Chat);
}
// RMI name declaration.
// It is the unique pointer that indicates RMI name such as RMI profiler.
const string RmiName_Chat="Chat";
       
const string RmiName_First = RmiName_Chat;
		public override Nettention.Proud.RmiID[] GetRmiIDList() { return Common.RmiIDList; } 
	}
}

